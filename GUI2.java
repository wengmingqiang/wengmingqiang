package GUI1;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class GUI2 {
	public static void main(String[] args) {

		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				createAndShowGUI();

			}

		});

	}

	protected static void createAndShowGUI() {
		JFrame frame = new JFrame("201621123043-翁明强");
		frame.setLayout(new BorderLayout());// 设置边框布局

		JPanel jp = new JPanel(new GridLayout(2, 2));// 设置两行两列的网格布局（对应账号、密码）

		frame.add(jp, BorderLayout.CENTER);//把面板jp放在中间位置

		JLabel j1 = new JLabel("           QQ");
		j1.setFont(new java.awt.Font("楷体", 1, 24));//设置字体
		jp.add(j1);

		JTextField account = new JTextField(25);
		jp.add(account); 
		
		JLabel j3 = new JLabel("          密码");
		j3.setFont(new java.awt.Font("楷体", 1, 24));
		jp.add(j3);

		JPasswordField password = new JPasswordField(25);//设置输入密码框
		jp.add(password);
		
		JPanel jp2 = new JPanel();

		frame.add(jp2, BorderLayout.SOUTH);//把面板jp2放在边框的南边
		Button b1 = new Button("登录");
		jp2.add(b1);

		b1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				if (account.getText().trim().length() == 0 || new String(password.getPassword()).trim().length() == 0) {
					JOptionPane.showMessageDialog(null, "账号密码不允许为空");//弹出提示框
					return;
				}
				if (account.getText().equals(new String(password.getPassword())))//账号密码相同时提示登录成功
					JOptionPane.showMessageDialog(null, "登录成功");
				else {
					JOptionPane.showMessageDialog(null, "用户名或密码错误");
				}

			}
		});

		frame.setVisible(true);//设置框架为可见
		frame.setSize(600, 400);//设置框架大小

	}

}
