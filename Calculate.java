package GUI1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public  class Calculate {
    
    public static String add(String a,String b){
        
        double x = Double.parseDouble(a);
        double y = Double.parseDouble(b);
        
         BigDecimal a1 = new BigDecimal(Double.toString(x));
         BigDecimal b1 = new BigDecimal(Double.toString(y));
         
         return (a1.add(b1)).toString();
         
        
    }
    
}

