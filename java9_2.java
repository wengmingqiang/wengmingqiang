package pta3;

import java.util.Scanner;
 class Person {
	
	private String name;
	private int age;
	private boolean gender;
	private int id;
	
    private static int count = 0;
    
    
    
    static {
    	
    	System.out.println("This is static initialization block");
    }
    
    {
    	System.out.println("This is initialization block, id is " + count);
    	
    	
    }
    
  public Person() {
	  System.out.println("This is constructor");
	  this.id = count;
	  count++;
	  System.out.printf("%s,%d,%b,%d%n",name,age,gender,id);
	

  }


public Person(String name, int age, boolean gender) {
	this.name = name;
	this.age = age;
	this.gender = gender;
	
	this.id = count;
	count++;
}



public String toString() {
	return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
}

}


public class java9_2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		Person[] persons = new Person[n];

		for (int i = 0; i < persons.length; i++) {

			Person person = new Person(sc.next(), sc.nextInt(), sc.nextBoolean());
			persons[i] = person;
			
		}

		for (int j = persons.length - 1; j >= 0; j--) {
			System.out.println(persons[j]);

		}

		Person a = new Person();
		
		
		System.out.println(a.toString());

		sc.close();

	}

}
