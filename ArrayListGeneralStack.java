package pta9;

import java.util.ArrayList;
import java.util.List;

public class ArrayListGeneralStack<E> implements GeneralStack<E> {


	List<E> List = new ArrayList<E>();
	
	
	public E push(E item) {
		if(item == null)
		   return null;
		List.add(  item);
		return item;
	}


	public E pop() {
		if(List.size() == 0)
			   return null;
		
		return List.remove(List.size()-1);
	}


	public E peek() {
		
		return List.get(List.size()-1);
	}

	
	public boolean empty() {
		return List.size()==0?true:false;
	}

	
	public int size() {
		
		return List.size();
	}
	
	
	public String toString() {
		return List.toString();
	}
	

}
