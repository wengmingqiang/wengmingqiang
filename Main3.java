package pta11;


/*6-3 jmu-Java-07多线程-Runnable与匿名类（3 分）
在Main方法中启动一个线程t1，该线程打印3行信息：
主线程名
线程t1的线程名
线程t1所实现的所有接口。提示：使用System.out.println(Arrays.toString(getClass().getInterfaces()));打印
注：本题也可使用Lambda表达式实现。*/


import java.util.Arrays;

public class Main3 {	
    public static void main(String[] args) {
        final String mainThreadName = Thread.currentThread().getName();
        Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println(mainThreadName);
				
				 System.out.println(Thread.currentThread().getName());
			
				
				
				
				System.out.println(Arrays.toString(getClass().getInterfaces()));
			}
		});
		
        t1.start();
    }
}