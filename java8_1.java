package pta;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class java8_1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[] strDigits;
		int[] digits = null;

		while (true) {
			String choice = sc.next();

			switch (choice) {
			case "fib":
				int n = sc.nextInt();
				for (int x = 1; x < n; x++) {
					System.out.print(fib(x) + " ");
				}
				System.out.println(fib(n));// 最后一个数特殊处理，最后一个不能带空格
				break;
			case "sort":
				sc.nextLine();// 赋值给回车，否则按完回车就会出现exit
				String line = sc.nextLine();
				strDigits = line.split(" ");// 以空格为分隔符，把其他部分存入String数组
				digits = covertToIntDigits(strDigits);// 构造一个函数把String数组转化为int型数组
				Arrays.sort(digits);// 对int行数组排序
				System.out.println(Arrays.toString(digits));// 按要求打印数组（用【】包括，元素用逗号隔开）

				break;
			case "search":
				sc.nextLine();// 赋值给回车，否则按完回车就会出现exit
				Arrays.sort(digits);
				int digit = sc.nextInt();
				int pos = Arrays.binarySearch(digits, digit);// 在数组中寻找制定的数，找到返回下标，否则返回一个负数
				if (pos < 0)
					System.out.println(-1);
				else
					System.out.println(pos);
				break;
			case "getBirthDate":
				sc.nextLine();// 赋值给回车，否则按完回车就会出现exit
				int x = sc.nextInt();
				for (int i = 0; i < x; i++) {

					String id = sc.next();
					System.out.println(getBirthDate(id));// 构造函数，获得年-月-日的字符串

				}

				break;
			default:
				System.out.println("exit");
				break;
			}
		}

	}

	private static String getBirthDate(String id) {

		StringBuilder birthdate = new StringBuilder(id.substring(6, 14));// 转化为StringBuilder对象，编译插入“-”
																			// id.substring(6,14)可裁剪字符串
		birthdate.insert(4, '-');
		birthdate.insert(7, '-');
		String BD = new String(birthdate);// 再把StringBuild转化为String型字符串
		return BD;

	}

	private static int[] covertToIntDigits(String[] strDigits) {

		int[] array = new int[strDigits.length];
		for (int i = 0; i < strDigits.length; i++) {
			array[i] = Integer.parseInt(strDigits[i]);// 字符串转化为int型数值
		}
		return array;
	}

	private static int fib(int n) {

		if (n == 1 || n == 2)
			return 1;
		else
			return fib(n - 1) + fib(n - 2);
	}

}
