package Shopping;

import java.util.Scanner;



public class test {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Goods g1 = new Goods("pen",2);
		Goods g2 = new Goods("apple",1);
		Goods g3 = new Goods("book",10);
		Goods g4 = new Goods("water",3);
		
		GoodsCity goodscity = new GoodsCity();
		
		goodscity.GoodsAdd(g1,3);
		goodscity.GoodsAdd(g2,4);
		goodscity.GoodsAdd(g3,5);
		goodscity.GoodsAdd(g4,6);
		
		ShoppingCar shoppingcar = new ShoppingCar();
		
		shoppingcar.GoodsAdd(g1, 3);
		shoppingcar.GoodsAdd(g2, 4);
		shoppingcar.GoodsAdd(g3, 7);
		shoppingcar.GoodsAdd(g4, 8);
		
		ShoppingCar.Display();

		
		
	}

}
