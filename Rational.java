package my;

class Rational {
	private int v1;
	private int v2;

	public Rational(int v1, int v2) {
	
		this.v1 = v1;
		this.v2 = v2;
	}

	public static int Gcd(int m,int n) {//求两个数最大公约数以便约分
		
		int t;
		while(m%n!=0)
		{
			t=n;
			n=m%n;
			m=t;
		}
		return n;

	}

	public static String Add( Rational T1,Rational T2) {//有理数相加函数
		
		int x1 = T1.v1*T2.v2+T2.v1*T1.v2;//分子
		int x2 = T1.v2*T2.v2; //分母
		int t = Gcd(x1,x2);
		x1/=t;
		x2/=t;
	
		return (x1+"/"+x2);

	}
	
	public static String Minus( Rational T1,Rational T2) {//有理数相减
		int x1 = T1.v1*T2.v2-T2.v1*T1.v2;//分子
		int x2 = T1.v2*T2.v2; //分母
		int t = Gcd(x1,x2);
		x1/=t;
		x2/=t;
		return (x1+"/"+x2) ;
		
	}
	
	public static String Multiply( Rational T1,Rational T2) {//有理数相乘
		int x1 = T1.v1 * T2.v1;
		int x2 = T1.v2 * T2.v2;
		int t = Gcd(x1,x2);
		x1/=t;
		x2/=t;
		
		return (x1+"/"+x2);
	
		
	}
	
	public static String Divide( Rational T1,Rational T2) {//有理数相除
		int x1 = T1.v1 * T2.v2;
		int x2 = T1.v2 * T2.v1;
		int t = Gcd(x1,x2);
		x1/=t;
		x2/=t;
		
		return (x1+"/"+x2);
	

     }

}



public class Rational {

	public static void main(String[] args) {

		Rational a = new Rational(1,2);
		Rational b = new Rational(1,3);
		
		System.out.println(Rational.Add(a,b));
		System.out.println(Rational.Minus(a,b));
		System.out.println(Rational.Multiply(a,b));
		System.out.println(Rational.Divide(a,b));
	}
}