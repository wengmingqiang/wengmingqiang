package pta9;



enum Gender{
	man,woman;	
};

public class Student {
	
	private Long id; 
	private String name; 
	private int age; 
	private Gender gender;//枚举类型 
	private boolean joinsACM; //是否参加过ACM比赛
	
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", joinsACM=" + joinsACM
				+ "]";
	}
	/**
	 * @param i
	 * @param name
	 * @param age
	 * @param gender
	 * @param joinsACM
	 */
	public Student(long i, String name, int age, Gender gender, boolean joinsACM) {
		super();
		this.id = i;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.joinsACM = joinsACM;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public boolean isJoinsACM() {
		return joinsACM;
	}
	public void setJoinsACM(boolean joinsACM) {
		this.joinsACM = joinsACM;
	}
	
	
	
	

}
