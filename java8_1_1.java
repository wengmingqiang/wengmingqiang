
package pta;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class java8_1_1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[]  strDigits;
		int[] digits = null;
		
		while(true) {
		String choice = sc.next();
		
		
		switch (choice) {
		case "fib":
			int n = sc.nextInt();
			fib(n);
			System.out.println();
			break;
		case "sort":
			sc.nextLine();
			String line = sc.nextLine();
			strDigits = line.split(" ");//-1 3 2 5 49
			digits = covertToIntDigits(strDigits);
			Arrays.sort(digits);
			System.out.println(Arrays.toString(digits));
			
			break;
		case "search":
			sc.nextLine();
			Arrays.sort(digits);
			int digit = Integer.parseInt(sc.nextLine());
			int pos = Arrays.binarySearch(digits,digit);
     		if(pos<0)
				System.out.println(-1);
			else
				System.out.println(pos);
			break;
		case "getBirthDate":
			sc.nextLine();
			int x = sc.nextInt();
			for(int i = 0; i<x;i++) {
				
				String id = sc.next();
				System.out.println(getBirthDate(id));
				
			}

			break;
		default:
			System.out.println("exit");
			break;
		}
		}

	}
	
	
	
	

	private static String getBirthDate(String id) {
		
		StringBuilder birthdate = new StringBuilder(id.substring(6,14));
		
		birthdate.insert(4, '-');
		birthdate.insert(7, '-');
		String BD = new String(birthdate);
		return BD;
		
		
	}

	private static int[] covertToIntDigits(String[] strDigits) {
		
		int[] array  = new int[strDigits.length];
		for(int i = 0; i<strDigits.length;i++)
		{
			array[i] = Integer.parseInt(strDigits[i]);
		}
		return array;
	}

	private static void fib(int n) {
		
		int b =1; int c = 1;
		int i = 0;
		while(i < n)
		{
			if(i==n-1)
				System.out.print(b);
			else {
			System.out.print(b+" ");
			int temp;
			temp = b;
			b = c;
			c = temp + c;
			}
			i++;
		}
		
		
	}

}
