package pta8;

import java.util.ArrayList;
import java.util.List;

public class ArrayIntegerStack implements IntegerStack {

	List<Integer>  ArrList = new ArrayList<Integer>();
	
	public Integer push(Integer item) {
		if(ArrList == null)
			return null;
		ArrList.add(item);
		return item;
	}

	
	public Integer pop() {
		if(ArrList.isEmpty())
			return null;
		
		return ArrList.remove(ArrList.size()-1);
	}

	
	public Integer peek() {
		if(ArrList.size()==0)
			return null;
		return ArrList.get(ArrList.size()-1);
	}


	public boolean empty() {
		return ArrList.size()==0?true:false;	
	}

	@Override
	public int size() {
	
		return ArrList.size();
	}


	public String toString() {
		return ArrList.toString();
	}
	
	
	
	

}
