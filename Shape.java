package pta3;

public   class Shape {
	final static double PI = 3.14;

	public static double SumPerimeter=0;
	public static double SumArea=0;
	
	public double sumAllArea(double a){
		
		SumArea += a;
		return SumArea;
	}
	public double sumAllPerimeter(double b){
		
		SumPerimeter += b;
		return SumPerimeter;
	}
}


class Circle extends Shape {
	
	private int radius;


	public Circle(int radius) {
		sumAllPerimeter(2*PI*radius);
		sumAllArea(PI*radius*radius);
		this.radius = radius;
	}
	
	public double getPerimeter() {
		
		return  (2*PI*radius);
	}
	
	public double getArea() {
		
		return  (PI*radius*radius);
	}


	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
}


class Rectangle extends Shape {
	
	private int width;
	private int length;
	
	
	public Rectangle(int width, int length) {
		sumAllPerimeter(2*(width+length));
		sumAllArea(width*length);
		this.width = width;
		this.length = length;
	}
	public double getPerimeter() {
		
		
		return (2*(width+length));
}
	
	public double getArea() {
	
		return (width*length);
	}

	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}

}
