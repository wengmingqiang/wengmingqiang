package pta8;

import java.util.ArrayList;
import java.util.List;

public class CharStack implements CharStackFace {

	
	
	List<Character>  ArrList = new ArrayList<Character>();
	
	
	public char push(char a) {
		
		if(ArrList == null)
			return 0;
		ArrList.add(a);
		return a;
		
	}


	public char pop() {
		if(ArrList.isEmpty())
			return 0;
		
		return ArrList.remove(ArrList.size()-1);
		
		
	}

	
	public char peek() {
		
		if(ArrList.size()==0)
			return 0;
		return ArrList.get(ArrList.size()-1);
	}


	@Override
	public String toString() {
		return ArrList.toString();
	}
		
		
}

	
	
	
	

