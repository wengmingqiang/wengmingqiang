package my;

import java.time.LocalDate;
import java.time.LocalTime;

public class time {

	public static void main(String[] args) {
		
		//示例1 如何 在Java 8中获取当天的日期
		LocalDate today1 = LocalDate.now();
		System.out.println("Today's Local date : " + today1); 
		//输出Today's Local date : 2017-10-01

		
		
		//示例2 如何在Java 8中获取当前的年月日
		LocalDate today = LocalDate.now(); 
		int year = today.getYear(); 
		int month = today.getMonthValue(); 
		int day = today.getDayOfMonth(); 
		System.out.printf("Year : %d Month : %d day : %d \t %n", year, month, day); 
		//输出Year : 2017 Month : 10 day : 1 	
		
		
		
		//示例3 在Java 8中如何获取某个特定的日期
		LocalDate dateOfBirth = LocalDate.of(2010, 01, 14); 
		System.out.println("Your Date of birth is : " + dateOfBirth); 
		//Output : Your Date of birth is : 2010-01-14
		
		//示例4 在Java 8中如何检查两个日期是否相等
		LocalDate date1 = LocalDate.of(2017, 10, 01);
		if(date1.equals(today)){ 
		    System.out.printf("Today %s and date1 %s are same date %n", today, date1); 
		} 
		 
		

		//示例6 如何在Java 8中获取当前时间
		LocalTime time = LocalTime.now(); 
		System.out.println("local time now : " + time);
		//Output 
		//local time now : 16:33:33.369 // in hour, minutes, seconds, nano seconds
		
		
		
		
	}

}
