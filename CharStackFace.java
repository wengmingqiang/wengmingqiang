package pta8;

public interface CharStackFace {
	
	public char push(char a);
	public char pop(); 
	public char peek();
	

}
