package pta11;

import java.util.ArrayList;
import java.util.List;



public class MyProducerConsumerTest {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		Repository repo = new Repository();
		Thread producer = new Thread(new Producer(repo,100));//放入100个
		Thread consumer = new Thread(new Consumer(repo,100));//取出100个
		producer.start();
		consumer.start();
		producer.join();
		consumer.join();
		
		System.out.format("main end!仓库还剩%d个货物%n",repo.size());
	}

}

class Repository {// 存放字符串的仓库
	private int capacity = 10;//仓库容量默认为10
	private List<String> repo = new ArrayList<String>();// repo(仓库)，最多只能放10个
//201621123043
	public synchronized void add(String t) throws InterruptedException {
		if (repo.size() >= capacity) {
			wait();
	
		} else {
		
			repo.add(t);
			notify();
		}
		
	}
	public synchronized void remove() throws InterruptedException {
		if (repo.size() <= 0) {
			wait();
			
		} else {
			
			repo.remove(0);
			notify();
		}
	}
	public synchronized int size(){
		return repo.size();
	}
}

class Producer implements Runnable {

	private Repository repo;
	private int count;//让Producer放入count次

	public Producer(Repository repo,int count) {
		this.repo = repo;
		this.count = count;
	}

	@Override
	public void run() {
		for (int i = 0; i < count; i++) {
			try {
			repo.add(new String("sth"));// 每回都放入一个新的货物(字符串对象)
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.format("放入%d个货物完毕!%n",count);
	}

}

class Consumer implements Runnable {
	private Repository repo;
	private int count;//让Consumer取count次

	public Consumer(Repository repo,int count) {
		this.repo = repo;
		this.count = count;
	}

	@Override
	public void run() {
		for (int i = 0; i < count; i++) {
			try {
			repo.remove();//每回都从仓库中取出一个货物
		
			}catch(InterruptedException e){
				e.printStackTrace();
				
				
			}
			
			}
		System.out.format("取出%d个货物完毕!%n",count);
	}

}
