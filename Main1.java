package pta11;

/*编写MyThread类继承自Thread。创建MyThread类对象时可指定循环次数n。
功能：输出从0到n-1的整数。 
并在最后使用System.out.println(Thread.currentThread().getName()+" "+isAlive())打印标识信息
*/
import java.util.Scanner;

class MyThread extends Thread {
	private int n;

	public MyThread(int n) {
		
		this.n = n;
	}
	
	public void run(){
		for(int i=0 ; i<n; i++){
			System.out.println(i);
			
		 }
		System.out.println(Thread.currentThread().getName()+" "+isAlive());
	}
	

}

public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Thread t1 = new MyThread(Integer.parseInt(sc.next()));
        t1.start();
    }
}