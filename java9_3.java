package pta3;

import java.util.Arrays;
import java.util.Scanner;

class Circle {
	
	private int radius;


	public Circle(int radius) {
		
		this.radius = radius;
	}
	
	public int getPerimeter() {
		return (int) (2*Math.PI*radius);
	}
	
	public int getArea() {
		return (int) (Math.PI*radius*radius);
	}


	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
}


class Rectangle {
	
	private int width;
	private int length;
	
	
	public Rectangle(int width, int length) {
		
		this.width = width;
		this.length = length;
	}
	public int getPerimeter() {
		
		return (2*(width+length));
	}
	public int getArea() {
		return (width*length);
	}

	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}

}


public class java9_3 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Rectangle [] a = new Rectangle[2];
		Circle [] b = new Circle[2];
		
		for(int i = 0; i<a.length; i++) {
			
			Rectangle a1 = new Rectangle(sc.nextInt(),sc.nextInt());
			a[i] = a1; 
		}
		

		for( int j  = 0; j<b.length; j++) {
			
			Circle b1 = new Circle(sc.nextInt());
			b[j] = b1; 
		}
		
	System.out.println(a[0].getPerimeter()+a[1].getPerimeter()+b[0].getPerimeter()+b[1].getPerimeter());
	System.out.println(a[0].getArea()+a[1].getArea()+b[0].getArea()+b[1].getArea());
	
	System.out.println(Arrays.deepToString(a));
	System.out.println(Arrays.deepToString(b));
	
		

	}

}
