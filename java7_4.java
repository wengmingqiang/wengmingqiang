package pta;

import java.util.Scanner;

public class java7_4 {

	public static void main(String[] args) {

		
		Scanner input = new Scanner(System.in);
		while(input.hasNext()) {
		double a = input.nextDouble();
		
		if(a<0)
			System.out.println("NaN");
		
		else
		{
		double x = 0;
		
	    while(x*x<a && Math.abs(a-x*x)>0.0001)
	    {
        	
	        x += 0.0001;
	    }
	    System.out.printf("%.6f\n",x);
		}
	}

}

	
}