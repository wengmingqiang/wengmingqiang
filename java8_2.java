package pta;

import java.util.Scanner;

public class java8_2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		while (true) {

			int n = sc.nextInt();

			StringBuilder str = new StringBuilder();// 使用StringBuild对象不会频繁产生新对象
			for (int i = 0; i < n; i++)
				str.append(i);

			int x = sc.nextInt();
			int y = sc.nextInt();

			System.out.println(str.substring(x, y));

		}
	}

}
