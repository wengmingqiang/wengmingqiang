package pta3;

import java.util.Arrays;
import java.util.Scanner;

public class java9_4 {

	public static void main(String[] args)
	{
		
		Scanner in = new Scanner(System.in);
		int n  = Integer.parseInt(in.nextLine());
		
		Shape[] Shapes = new Shape[n];
		
		for(int i=0; i < Shapes.length; i++)
		{
			
			String line = in.nextLine();
			if(line.equals("rect"))
			{
				String num[] = in.nextLine().split(" ");
				Shapes[i] = new Rectangle(Integer.parseInt(num[0]),Integer.parseInt(num[1]));
			}
			
			if(line.equals("cir"))
				Shapes[i] = new Circle(Integer.parseInt(in.nextLine()));
			
		}
		Shape s = new Shape();
		System.out.println(s.SumPerimeter);
		System.out.println(s.SumArea);
		System.out.println(Arrays.deepToString(Shapes));
		
		
		for (int j = 0;j<n; j++)
		{
			System.out.printf("%s,%s\n",Shapes[j].getClass(),Shapes[j].getClass().getSuperclass());
		}
		

	}

}
