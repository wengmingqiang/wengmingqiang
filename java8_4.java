package pta;

import java.util.Arrays;
import java.util.Scanner;

public class java8_4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		while (true) {

			int n = sc.nextInt();

			String[][] arr = new String[n][];//建立一个n行二维数组

			for (int x = 0; x < n; x++) // 建立锯齿的二维数组
				arr[x] = new String[x + 1];

			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= i; j++) {
					arr[i - 1][j - 1] = i + "*" + j + "=" + i * j;// 给数组元素赋值

					if (j == i)// 对每一行最后一个特殊处理
						System.out.printf("%s", arr[i - 1][j - 1]);
					else
						System.out.printf("%-7s", arr[i - 1][j - 1]);// %-7s是左对齐在右边填充空格，使之达到7个字符
				} // %7s是右对齐在左边填充空格
				System.out.println();
			}

			System.out.println(Arrays.deepToString(arr));// 打印二维数组
		}
	}

}
