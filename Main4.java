package pta10;

import java.util.Scanner;

class IllegalScoreException extends Exception{
	public IllegalScoreException(){
			
		}
	public IllegalScoreException(String s){
			super(s);
		}
}

class IllegalNameException extends Exception{
	public IllegalNameException(){
		
	}
	public IllegalNameException(String s){
		super(s);
	}
}



class Student{
	private String name;
	private int score;
	public String getName() {
		return name;
	}
	public void setName(String name) throws IllegalNameException {
		char a[]=name.toCharArray();
		if(a[0]>47&&a[0]<58)
			throw new IllegalNameException("the first char of name must not be digit, name="+name);
		this.name=name;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}
	public int addScore(int score) throws IllegalScoreException{
		this.score = score;
		if(this.score<0||this.score>100)
			throw new IllegalScoreException("score out of range, score="+score);
		return this.score ;
	}
}

public class Main4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		String x = in.next();
		while(x.equals("new")){
			try{
				in.nextLine();
				Student s = new Student();
				String a = in.nextLine();
				String a1[] = a.split(" ");
				if(a1.length<2){
					System.out.println("java.util.NoSuchElementException");
					x = in.next();
					continue;
				}
				s.setName(a1[0]);
				s.addScore(Integer.parseInt(a1[1]));
				System.out.println(s.toString());
				x = in.next();	
			}
			catch(IllegalNameException e){
				System.out.println(e);
				x = in.next();
				continue;
			}
			catch(IllegalScoreException e){
				System.out.println(e);
				x = in.next();
				continue;
			}
			catch(Exception e){
				System.out.println(e);
				x = in.next();
				continue;
			}
			
		}
		System.out.println("scanner closed");
	}

}
